Exercise: Install and Configure PostgreSQL 9.x: RHEL/CentOS

1. Find the PostgreSQL 9.4 repository RPM from the appropriate website and either download and install it using the appropriate package management commands or install directly from the website link.

  sudo yum install https://download.postgresql.org/pub/repos/yum/9.4/redhat/rhel-7-x86_64/pgdg-centos94-9.4-3.noarch.rpm

2. Update your system after installing the repository update in Step #1, applying any necessary updates.

	yum update

3. Using the appropriate package management command, install the PostgreSQL 9.4 server and associated contribed modules and utilities. Once installed, run the database initialization routine before starting the database.

	yum install postgresql94-server postgresql94-contrib
	
	 /usr/pgsql-9.4/bin/postgresql94-setup initdb


4. Using the appropriate SYSTEMD service management commands, enable the PostgreSQL 9.4 server to run on system start and then start the database server.

    systemctl enable postgresql-9.4
    
    systemctl start postgresql-9.4

5. Check to see if SELinux is being run in enforced mode on your system. If so, run the command to allow external HTTP DB connections to the server through SELinux configuration.

    cat /etc/selinux/config

    setsebool -P httpd_can_network_connect_db 1

 

 

6. Become the 'postgres' user and run the 'psql' command. Once at the database prompt, set a password for the 'psql' user. 

    sudo su - postgres

    -bash-4.2$ psql

    psql (9.4.19)

    Type "help" for help.


    postgres=# \password postgres

    postgres=# quit

    postgres-# \q

    -bash-4.2$ exit

    logout
